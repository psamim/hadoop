package org.bihe.bigdata.samim;

import java.io.*;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class Tokenizer {
    public static void main(String[] args) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(new File(args[0]));
            //Construct BufferedReader from InputStreamReader
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));

            String line = null;
            while ((line = br.readLine()) != null) {
                GetTokens(line);
            }

            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void GetTokens(String line) {
        StringTokenizer itr = new StringTokenizer(line);
        while (itr.hasMoreTokens()) {
            String nextToken = itr.nextToken();
            nextToken = nextToken.replaceAll("[^a-zA-Z ]", "").toLowerCase();
            System.out.println(nextToken);
        }
    }
}
